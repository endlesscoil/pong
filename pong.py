from kivy.app import App
from kivy.uix.widget import Widget
from kivy.graphics import Ellipse, PushMatrix, PopMatrix, Rotate
from kivy.properties import NumericProperty, ReferenceListProperty, ObjectProperty, ListProperty
from kivy.vector import Vector
from kivy.clock import Clock
from kivy.logger import Logger
from kivy.core.audio import SoundLoader
from random import randint


class PongBall(Widget):
    # velocity of the ball on x and y axis
    velocity_x = NumericProperty(0)
    velocity_y = NumericProperty(0)
    rotation = NumericProperty(0)
    rotation_speed = NumericProperty(5)

    # refrencelist property so we can use ball.velocity
    # as a shorthand, just like e.g. w.pos for w.x and w.y
    velocity = ReferenceListProperty(velocity_x, velocity_y)

    # ``move`` function will move the ball one step.  This
    # will be called in equal intervals to animate the ball
    def move(self):
        self.pos = Vector(*self.velocity) + self.pos
        self.rotation += self.rotation_speed
        # if self.rotation > 360:
        #     self.rotation = 1


class PongPaddle(Widget):
    score = NumericProperty(0)

    def __init__(self, **kwargs):
        super(PongPaddle, self).__init__(**kwargs)

        self._ahhh = SoundLoader.load('ahhh.wav')
        self._ahhh.volume = 0.5

    def bounce_ball(self, ball):
        if self.collide_widget(ball):
            vx, vy = ball.velocity
            
            # Change angle of ball based on position of paddle
            offset = (ball.center_y - self.center_y) / (self.height / 2)
            
            # Reverse x velocity
            bounced = Vector(-1 * vx, vy)
            
            # Speed up the ball
            vel = bounced * 1.1

            # Speed up rotation and reverse it's direction
            ball.rotation_speed *= -1.1

            # Set new velocity and speed things up.
            ball.velocity = vel.x, vel.y + offset

            self._ahhh.play()


class PongBlood(Widget):
    stage = NumericProperty(0)

    def __init__(self, ball, animate_speed=2):
        super(PongBlood, self).__init__()
        self.size = (100, 100)
        self.animate_speed = animate_speed
        self.counter = 0

    def show(self, angle, pos):
        x, y = pos

        if angle == 270:
            x = 0 + self.parent.BLADE_WIDTH / 4
        elif angle == 90:
            x = self.parent.width - self.parent.BLADE_WIDTH / 4

        self.pos = (x - self.width / 2, y)

        with self.canvas.before:
            PushMatrix()
            Rotate(angle=angle, axis=(0, 0, 1), origin=self.center)

        with self.canvas:
            Ellipse(pos=self.pos, size=self.size, source='atlas://blood/0')

        with self.canvas.after:
            PopMatrix()

    def update(self):
        self.counter += 1

        if (self.counter % self.animate_speed) == 0:
            self.stage += 1
            if self.stage > 5:
                self.stage = -1
            else:
                with self.canvas:
                    Ellipse(pos=self.pos, size=self.size, source='atlas://blood/{0}'.format(str(self.stage)))


class SawBlade(Widget):
    rotation = NumericProperty(0)
    rotation_speed = NumericProperty(5)

    def update(self):
        self.rotation += self.rotation_speed


class PongGame(Widget):
    SCORE_SIZE = 70
    BLADE_WIDTH = 50

    ball = ObjectProperty(None)
    player1 = ObjectProperty(None)
    player2 = ObjectProperty(None)
    blood = ListProperty(None)

    def __init__(self, **kwargs):
        super(PongGame, self).__init__(**kwargs)

        self.size = (800, 600)

        self._saw_sound = SoundLoader.load('saw.wav')
        self._saw_sound.loop = True
        self._saw_sound.volume = 0.4

        self.blades = []
        self.add_blades()

        self._saw_sound.play()

    def update(self, dt):
        # call ball.move and other stuff
        self.ball.move()

        self.update_blades()
        self.update_blood()

        # bounce ball off paddles
        self.player1.bounce_ball(self.ball)
        self.player2.bounce_ball(self.ball)

        # bounce off top and bottom
        if self.ball.y < 0 or self.ball.top > self.height:
            self.ball.velocity_y *= -1

        if self.ball.center_x < self.x + self.BLADE_WIDTH / 2:
            self.player2.score += 1
            self.add_blood(angle=270)
            self.serve_ball(vel=(4, 0))

        if self.ball.center_x > self.width - self.BLADE_WIDTH / 2:
            self.player1.score += 1
            self.add_blood(angle=90)
            self.serve_ball(vel=(-4, 0))

    def on_touch_move(self, touch):
        if touch.x < self.width / 3:
            self.player1.center_y = touch.y

        if touch.x > self.width - self.width / 3:
            self.player2.center_y = touch.y

    def serve_ball(self, vel=(4, 0), rotation_speed=5):
        self.ball.center = self.center
        self.ball.velocity = vel

        if vel[0] > 0:
            rotation_speed *= -1

        self.ball.rotation_speed = rotation_speed

    def add_blades(self):
        for c in range(2):
            x = (self.width * c) - self.BLADE_WIDTH / 2
            y = 0

            while True:
                sawblade = SawBlade()
                sawblade.pos = (x, y)

                self.blades.append(sawblade)
                self.add_widget(sawblade)

                y += sawblade.height
                if y > self.height:
                    break

    def update_blades(self):
        for blade in self.blades:
            blade.update()

    def add_blood(self, angle=0):
        blood = PongBlood(self.ball)
        self.add_widget(blood)
        self.blood.append(blood)

        blood.show(angle=angle, pos=self.ball.pos)

    def update_blood(self):
        for idx, b in enumerate(self.blood):
            b.update()
            if b.stage == -1:
                self.blood.remove(b)
                self.remove_widget(b)

class PongApp(App):
    def build(self):
        game = PongGame()
        game.serve_ball()

        # Update game 60 times per second.
        Clock.schedule_interval(game.update, 1.0 / 60.0)

        return game


if __name__ == '__main__':
    PongApp().run()